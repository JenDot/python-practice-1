melons = {
    'Honeydew': {'Price': 0.99,
                 'Seeds?': True,
                 'Flesh Color': None},
    'Crenshaw': {'Price': 2.00,
                 'Seeds?': False,
                 'Flesh Color': None},
    'Crane': {'Price': 2.50,
              'Seeds?': False,
              'Flesh Color': None},
    'Casaba': {'Price': 2.50,
               'Seeds?': False,
               'Flesh Color': None}
}
