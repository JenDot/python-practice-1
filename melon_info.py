"""Print out all the melons in our inventory."""


from melons import melons as Melons


def print_melon(melons):
    """Print each melon with corresponding attribute information."""

    for melon_name, traits in melons.items():
        print(melon_name.upper())

        for trait, value in traits.items():
            print(f'{trait}: {value}')


print_melon(Melons)
